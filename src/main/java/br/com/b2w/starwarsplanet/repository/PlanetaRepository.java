package br.com.b2w.starwarsplanet.repository;

import br.com.b2w.starwarsplanet.model.Planeta;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PlanetaRepository extends MongoRepository<Planeta, String> {

    /**
     * Busca pelo nome
     * @param nome String
     * @return Planeta
     */
    Planeta findByNome(@Param("nome") String name);
}
