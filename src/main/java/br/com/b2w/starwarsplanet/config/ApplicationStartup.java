package br.com.b2w.starwarsplanet.config;

import br.com.b2w.starwarsplanet.model.Planeta;
import br.com.b2w.starwarsplanet.repository.PlanetaRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    private final String SWAPI_API = "http://swapi.co/api/planets";

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    PlanetaRepository planetaRepository;

    /**
     * Faz uma carga inicial no banco
     * @param applicationReadyEvent
     */
    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        /*ObjectNode objectNode = restTemplate.getForObject(SWAPI_API, ObjectNode.class);
        JsonNode results = objectNode.get("results");*/
        String objectNode = restTemplate.getForObject(SWAPI_API, String.class);
        JsonNode results = null;

        try {
            results = new ObjectMapper().readTree(objectNode).get("results");
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (results.isArray()) {
            //Remove todos os dados ao iniciar
            planetaRepository.deleteAll();

            for (final JsonNode objNode : results) {
                Planeta p = new Planeta();
                p.setNome(objNode.get("name").asText());
                p.setClima(objNode.get("climate").asText());
                p.setTerreno(objNode.get("terrain").asText());
                p.setQtdAparicoes(objNode.get("films").size());

                planetaRepository.save(p);
            }
        }
    }
}
