package br.com.b2w.starwarsplanet.controller;

import br.com.b2w.starwarsplanet.model.Planeta;
import br.com.b2w.starwarsplanet.repository.PlanetaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PlanetaController {

    @Autowired
    PlanetaRepository planetaRepository;

    /**
     * Mostra todos os dados que existem no banco
     * @return List<Planeta>
     */
    @RequestMapping(method = RequestMethod.GET, value = "/planetas")
    public ResponseEntity<List<Planeta>> getAll() {
        return new ResponseEntity<>(planetaRepository.findAll(), HttpStatus.OK);
    }

    /**
     * Busca um planeta pelo seu id
     * @param id String
     * @return Planeta
     */
    @RequestMapping(method = RequestMethod.GET, value = "/planeta/id/{id}")
    public ResponseEntity<Planeta> getById(@PathVariable("id") String id) {
        Planeta optionalPlaneta = planetaRepository.findById(id).orElse(null);

        HttpStatus retorno = optionalPlaneta != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;

        return new ResponseEntity<>(optionalPlaneta != null ? optionalPlaneta : null, retorno);
    }

    /**
     * usca um planeta pelo seu nome
     * @param nome String
     * @return Planeta
     */
    @RequestMapping(method = RequestMethod.GET, value = "/planeta/nome/{nome}")
    public ResponseEntity<Planeta> getByNome(@PathVariable("nome") String nome) {
        Optional<Planeta> optionalPlaneta = Optional.ofNullable(planetaRepository.findByNome(nome));

        HttpStatus retorno = optionalPlaneta.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND;

        return new ResponseEntity<>(optionalPlaneta.isPresent() ? optionalPlaneta.get() : optionalPlaneta.orElse(null), retorno);
    }

    /**
     * Remove um planeta pelo seu ID
     * @param id String
     * @return ResponseEntity
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/planeta/remover/{id}")
    public ResponseEntity removeById(@PathVariable("id") String id) {
        Planeta planeta = planetaRepository.findById(id).orElse(null);
        HttpStatus retorno = HttpStatus.OK;

        if(planeta != null)
            planetaRepository.delete(planeta);
        else
            retorno = HttpStatus.NOT_FOUND;

        return new ResponseEntity(retorno);
    }

    /**
     * Grava um novo planeta
     * @param planeta Planeta
     * @return ResponseEntity
     */
    @RequestMapping(method = RequestMethod.POST, value = "/planeta/salvar")
    public ResponseEntity save(@RequestBody Planeta planeta) {
        Optional<Planeta> optionalPlaneta = Optional.of(planetaRepository.save(planeta));

        return new ResponseEntity(optionalPlaneta.isPresent() ? HttpStatus.CREATED : HttpStatus.BAD_REQUEST);
    }
}
