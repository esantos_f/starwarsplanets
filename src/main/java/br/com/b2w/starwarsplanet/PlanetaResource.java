package br.com.b2w.starwarsplanet;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

public class PlanetaResource extends ResourceSupport {

    private final String content;

    @JsonCreator
    public PlanetaResource(@JsonProperty("content") String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
