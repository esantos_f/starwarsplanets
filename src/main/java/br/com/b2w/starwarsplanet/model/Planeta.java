package br.com.b2w.starwarsplanet.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Planeta {

    @Id
    private String id;
    private String nome;
    private String clima;
    private String terreno;
    private int qtdAparicoes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getClima() {
        return clima;
    }

    public void setClima(String clima) {
        this.clima = clima;
    }

    public String getTerreno() {
        return terreno;
    }

    public void setTerreno(String terreno) {
        this.terreno = terreno;
    }

    public int getQtdAparicoes() {
        return qtdAparicoes;
    }

    public void setQtdAparicoes(int qtdAparicoes) {
        this.qtdAparicoes = qtdAparicoes;
    }
}