package br.com.b2w.starwarsplanet.controller;

import br.com.b2w.starwarsplanet.model.Planeta;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PlanetaController.class)
public class PlanetaControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PlanetaController planetaController;

    @Test
    public void getAll() throws Exception {
        Planeta p = new Planeta();
        p.setNome("Teste V");
        List<Planeta> planetas = singletonList(p);

        given(planetaController.getAll()).willReturn(new ResponseEntity<List<Planeta>>(planetas, HttpStatus.OK));

        mvc.perform(get("/planetas")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].nome", is(p.getNome())));
    }

    @Test
    public void getById() throws Exception {
        Planeta p = new Planeta();
        p.setId("1");

        when(planetaController.getById("1")).thenReturn(new ResponseEntity<Planeta>(p, HttpStatus.OK));

        mvc.perform(get("/planeta/id/{id}", "1")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(p.getId())));
    }

    @Test
    public void getByNome() throws Exception {
        Planeta p = new Planeta();
        p.setNome("Teste V");

        when(planetaController.getByNome("Teste V")).thenReturn(new ResponseEntity<Planeta>(p, HttpStatus.OK));

        mvc.perform(get("/planeta/nome/{nome}", "Teste V")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nome", is(p.getNome())));
    }

    @Test
    public void removeById() throws Exception {
        Planeta p = new Planeta();
        p.setId("1");

        mvc.perform(delete("/planeta/remover/{id}", "1")
                .contentType("application/json"))
                .andExpect(status().isOk());
    }
}
